'use strict';

// requirements
const express = require('express');
const { User, ValidationError } = require('./user')

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

app.get('/', (req, res) => {
    res.send('Post user preferences: name and email');
});

app.post('/', (req, res) => {
    try {
        const params = Object.keys(req.body)
        const { email, name } = req.body
        if (params.filter(v => !['name', 'email'].includes(v)).length !== 0
            || email == null
            || name == null
            ) {
            res.status(400).send('Please provide both email and name');
            return;
        }

        console.log(new User({email, name}).toString());
        res.send('Your preferences have been successfully saved');
    } catch (e) {
        if(e instanceof ValidationError) {
            return res.status(400).send('Invalid request');
        }
        throw e
    }
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
