const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
class ValidationError extends Error {}

class Validatable {
  isValid() {
    return false;
  }

  validate() {
    if (this.isValid()) {
      Object.freeze(this)
      return this;
    } else {
      throw new ValidationError("Entity was not valid");
    }
  }
}

class Email extends Validatable {
  constructor(val) {
    super();
    this._val = val;
    this.validate();
  }

  isValid() {
    return (
      typeof this._val === "string" &&
      this._val.length > 4 &&
      this._val.length < 200 &&
      emailRegex.test(this._val)
    );
  }

  toString() {
    return `Email { ${this._val} }`;
  }
}

class Name extends Validatable {
  constructor(val) {
    super();
    this._val = val;
    this.validate();
  }

  isValid() {
    return (
      typeof this._val === "string" &&
      this._val.length < 200 &&
      this._val.length > 4
    );
  }

  toString() {
    return `Name { ${this._val} }`;
  }
}

class UserID extends Validatable {
  constructor(val) {
    super();
    this._val = val;
    this.validate();
  }

  isValid() {
    return typeof this._val === "number" && this._val > 0;
  }
}

class UserRole extends Validatable {
  constructor(val) {
    super();
    this._val = val;
    this.validate();
  }

  isValid() {
    return typeof this._val === "string" && ["user"].includes(this._val);
  }
}

class User extends Validatable {
  constructor({ name, email, id = auth_user_id(), role = "user" }) {
    super();
    this._id = new UserID(id);
    this._role = new UserRole(role);
    this._name = new Name(name);
    this._email = new Email(email);
    this.validate();
  }

  isValid() {
    return this._id != null && this._email != null;
  }

  get id() {
    return this._id;
  }

  get role() {
    return this._role;
  }

  get name() {
    return this._name;
  }

  get email() {
    return this._email;
  }

  toString() {
    return `User {
      id: ${this.id.toString()}
      role: ${this.role.toString()}
      email: ${this.email.toString()}
      name: ${this.name.toString()}
    }`;
  }
}

// A psudo authenticator
// returns authenticated user ID
function auth_user_id() {
  return 2;
}

module.exports = { User, ValidationError, auth_user_id };
